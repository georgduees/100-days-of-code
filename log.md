# 100 Days Of Code - Log

### Day 0: February 12, 2018

**Today's Progress**: Wanted to post my updates to Twitter, so thought about creating a Twitter-Bot which convertes my Commit-Messages to any of my 100 Days of code related repositories via my Twitter handle.

**Thoughts:** Created a bulk webapi project and set up my dev environment. created a Twitter App and Learned about Webhook usaage and Github
              

**Link to work:** [commitTweetApp](http://github.com/georgduees/commitTweetApp)

### Day 01: February 13, 2018

**Today's Progress**: Created a .Net Core WebApi project with first "layout" and leaned about authentication of Twitter and GitHub Webhooks

**Thoughts:** I came to the conclusiont hat learning is more reading than writing code... 

**Link to work:** [commitTweetApp](http://github.com/georgduees/commitTweetApp)

### Day 02: February 14, 2018

**Today's Progress**: Wanted to update further but got stuck a bit as there was "Too Much Work to Do"... came to the conclusion to make the project available for everybody, so this will be a bit bigger WEBPROJECT than expected at first. Came up with a DB Schema which will suit the Idea.

**Thoughts:** Once you get started i mostly find myself in digging up information and reading about different ways of building things than doing them really.

**Link to work:** [commitTweetApp](http://github.com/georgduees/commitTweetApp)

### Day 03: February 15, 2018

**Today's Progress**: Updated the Database Schema and defined a Tasklist, which can be seen in the projects readme.

**Thoughts:** I rethought and came to a better naming and architecture to further expand the project later and make this one public.

**Link to work:** [commitTweetApp](http://github.com/georgduees/commitTweetApp)

## Day 04: February 15, 2018

**Today's Progress**: Created a Database Schema for today, tomorrow enable VersionControl on the Database vian webhooks.

**Helpful Links** https://stackoverflow.com/questions/5518349/using-git-to-track-mysql-schema-some-questions

**Link to work:** [ERD](https://github.com/georgduees/100-days-of-code-00-commitTweetApp/blob/master/db/layout_v0_0_1.svg)

### Day 05: February 16, 2018

**Today's Progress**: Enabled automatic SQLDumps pre-commit so now we have Versioncontrol of the database \o/ and checked yesterdays DB-design

**Helpful Links** https://stackoverflow.com/questions/5518349/using-git-to-track-mysql-schema-some-questions

**Link to work:** [If you want to see the latest database](https://github.com/georgduees/100-days-of-code-00-commitTweetApp/blob/master/db/dispatch.sql)


**Planned for tomorrow**: stored procedures and triggers for common actions of the tool.
